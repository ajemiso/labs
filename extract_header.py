"""
>>> extract_domain("spf=pass (google.com: domain of jwackman@hvc.rr.com designates 2\
a00:1450:400c:c09::22d as permitted sender")
hvc.rr.com

extract_domain("spf=pass (google.com: domain of jwackman@hvc.net designates 2\
a00:1450:400c:c09::22d as permitted sender")
hvc.net
"""

def extract_domain(string):
    location = string.find("@")
    new_string = string[location+1:]
    new_string = new_string.split()
    new_string = new_string[0]
    print(new_string)

def extract_domain(email_header):
    start = email_header.find("@")
    end = email_header.find(" ", start)
    email_header = email_header[start+1:end]
    print(email_header)
