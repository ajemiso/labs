"""
# Return a list of ints, that represent the position of all elements
# in a list where the numbers before and the number after are less than the number itself.
>>> peaks([1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 5, 6, 7, 8, 9, 8, 7, 6, 7, 8, 9])
[6, 14]

>>> valleys([1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 5, 6, 7, 8, 9, 8, 7, 6, 7, 8, 9])
[9, 17]

>>> peaks_and_valleys([1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 5, 6, 7, 8, 9, 8, 7, 6, 7, 8, 9])
[6, 9, 14, 17]


thing for thing in

# Return a list of lists that are sections of the input list sliced by the index values of peaks_and_valleys()
>>> chop([1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 5, 6, 7, 8, 9, 8, 7, 6, 7, 8, 9])
[[1, 2, 3, 4, 5, 6, 7], [6, 5, 4], [5, 6, 7, 8, 9], [8, 7, 6], [7, 8, 9]]
"""

def peaks(peak_list):
    new_list = []
    for index, item in enumerate(peak_list):
        try:
            if peak_list[index-1] < peak_list[index] and peak_list[index+1] < peak_list[index]:
                new_list.append(index)
        except IndexError:
            pass
    return new_list


def valleys(peak_list):
    new_list = []
    for index, item in enumerate(peak_list):
        if index > 0:
            try:
                if peak_list[index-1] > peak_list[index] and peak_list[index+1] > peak_list[index]:
                    new_list.append(index)
            except IndexError:
                pass
    return new_list

def peaks_and_valleys(peak_list):
    new_list = []
    for index, item in enumerate(peak_list):
        if index > 0:
            try:
                if peak_list[index-1] < peak_list[index] and peak_list[index+1] < peak_list[index]:
                    new_list.append(index)
                elif peak_list[index-1] > peak_list[index] and peak_list[index+1] > peak_list[index]:
                    new_list.append(index)
            except IndexError:
                pass
    return new_list


def chop(peak_list):
    new_list = []
    index_list = peaks_and_valleys(peak_list)
    for index, item in enumerate(index_list):
        try:
            mini_list = peak_list[:index_list[item]]
            new_list.append(mini_list)
        except IndexError:
            pass
    return new_list
