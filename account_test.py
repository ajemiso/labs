import unittest
from account import Account



class TestAccountCreation(unittest.TestCase):

    def setUp(self):
        self.test_account = Account(500)

    def tearDown(self):
        pass

    def test_initial_account(self):
        self.assertEqual(self.test_account.balance, 500)
        self.assertEqual(self.test_account.rate, 0.01)

    def test_withdrawal(self):
        with self.assertRaises(ValueError):
            self.test_account.withdrawal(10000000)

    def test_deposit(self):
        self.assertEqual(self.test_account.deposit(1700), 2200)

    def test_interest_calc(self):
        self.assertEqual(self.test_account.calc_interest(), (self.test_account.balance*self.test_account.rate)/12)

    def test_check_withdrawal(self):
        self.assertEqual(self.test_account.check_withdrawal(3032482034), False)