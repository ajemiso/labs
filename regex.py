import sys
import re

filename = sys.argv[1]

def reg_function(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        text = f.read()
        return text

def date_search():
    dates_times = []
    text = reg_function(filename)
    pattern = re.compile('\w..\s\d+\s\d.:\d+:\d+')
    dates = pattern.search(text)
    dates_times.append(dates)
    print(dates_times)

date_search()
