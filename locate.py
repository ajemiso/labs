"""
>>> locate('l', 'hello')
[2, 3]
"""

def locate(letter, word):
    lst = []
    start = word.find(letter)
    end = word.find(letter, start+1)
    lst.append(start)
    lst.append(end)
    print(lst)
