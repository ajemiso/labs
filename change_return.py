"""Calculate change.

Calculate the smallest number of coins needed to represent an amount of cents
less than 100.

>>> make_change(94)
3 quarters
1 dimes
1 nickles
4 pennies

"""
def make_change(amount):
    int(amount)
    if amount < 100:
        if amount > 25:
            quarters = amount // 25
            amount = amount - (25 * quarters)
            print("{} quarters".format(quarters))
        if amount > 10 and amount != 0:
            dimes = amount // 10
            amount = amount - (10 * dimes)
            print("{} dimes".format(dimes))
        if amount > 5 and amount != 0:
            nickels = amount // 5
            amount = amount - (5 * nickels)
            print("{} nickels".format(nickels))
        if amount > 1 and amount != 0:
            pennies = amount
            print("{} pennies".format(pennies))
    return "Amount entered is more than $1.00"
