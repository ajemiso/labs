"""
Write a function that returns true if the input is a palindrome, or false, if it is not.

>>> palindrome('hannah')
True
>>> palindrome('racecar')
True
>>> palindrome('a man, a plan, a canal, Panama')
True
>>> palindrome("1 pound coconut.")
False
>>> palindrome(1234321)
True
"""

def palindrome(word):
    word = str(word)
    word = word.replace(" ", "")
    word = word.lower()
    word = word.replace(",", "")
    if word == word[::-1]:
        return True
    else:
        return False
