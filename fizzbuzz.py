"""
Given the length of the output of numbers from 1 - n:
If a number is divisible by 3, append "Fizz" to a list.
If a number is divisible by 5, append "Buzz" to that same list.
If a number is divisible by both 3 and 5, append "FizzBuzz" to the list.
If a number meets none of theese rules, just append the string of the number.

>>> fizz_buzz(10)
['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz']

>>> joined_buzz(15)
'1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz'

"""

def fizz_buzz(number):
    f = "Fizz"
    b = "Buzz"
    lst = []
    for i in range(1, number+1):
        if i % 3 == 0 and i % 5 == 0:
            lst.append(f+b)
        elif i % 3 == 0:
            lst.append(f)
        elif i % 5 == 0:
            lst.append(b)
        else:
            lst.append(str(i))
    return lst


def joined_buzz(number):
    output = fizz_buzz(number)
    output = " and ".join(output)
    return output

print(fizz_buzz(10))
print(joined_buzz(15))
