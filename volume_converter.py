"""
I have been tasked with writing a program that asks the user
for a volume in gallons, converts it to liters, then prints it out.
"""

gallons = input("Please enter volume in gallons: ")

def volume_converter(gallons):
    conversion = 3.78541
    liters = float(gallons) * conversion
    print(liters)

#volume_converter(gallons)
