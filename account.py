"""

Save your solution in a directory in practice/ named atm-interface.

An account will be a class named Account in a module named account: it will have private attributes for the balance and
interest rate. Remember to underscore _ prefix any private attributes. A newly-instantiated account will have zero
balance and an interest rate of 0.1%.

Write class methods in the account class that:

get_funds() Return account balance deposit(amount) Deposit to the account
check_withdrawal(amount) Return True if large enough balance for a withdrawal
withdraw(amount) Withdraw an allowed amount; raise a ValueError if insufficient balance
calc_interest() Calculate and return interest on the current account balance

"""

import random

class Account:

    def __str__(self):
        return "Account: {0.account_number} Balance: {0.balance}".format(self)


    def __init__(self, balance, *args, **kwargs):
        # set up account number
        digits = list(range(10))
        digit_list = list()
        count = 0
        while count < 9:
            choice = str(random.choice(digits))
            digit_list.append(choice)
            count += 1

        #initialize vars
        self.account_number = int("".join(digit_list))
        self.balance = balance
        self.rate = 0.01


    def get_account(self):
        return self.account_number

    @property
    def get_funds(self):
        """ Return account balance """
        return self.balance

    def deposit(self, amount):
        """ Deposit to the account """
        amount = int(amount)
        self.balance += amount
        return self.balance

    def check_withdrawal(self, amount):
        """ Return True if large enough balance for a withdrawal """
        if amount < self.balance:
            return True
        else:
            return False

    def withdrawal(self, amount):
        """ Withdraw an allowed amount; raise a ValueError if insufficient balance """
        if amount > self.balance:
            raise ValueError("Insufficient funds.")
        else:
            self.balance -= amount
            return self.balance

    def calc_interest(self):
        """ Calculate and return interest on the current account balance """
        interest = (self.balance * self.rate) / 12
        return interest
