"""
# Given an input list, return it in reverse.
>>> backwards([56, 57, 58, 59, 60])
[60, 59, 58, 57, 56]


# Find the max number in a given list.  Then, change every element in the list containing the first number of the maximum to the maximum number.
>>> maximus([56, 57, 58, 59, 60])
[60, 57, 58, 59, 60]

>>> maximus([56, 67, 56, 59, 60])
[67, 67, 67, 59, 67]


# Given two lists, return True if the first two items are equal.
>>> two_lists(['migratory', 'birds', 'fly', 'south'], ['migratory', 'monopoloy', 'mind'])
True

# Return True if the first letter of the second element in the list is the same. (case insensitive)
>>> compare_second_letter(['migratory', 'penguins', 'fly', 'south'], ['One', 'Pound', 'Coconut'])
True

>>> compare_second_letter(['migratory', 'birds', 'fly', 'south'], ['One', 'Pound', 'Coconut'])
False


# Given two lists, return one list, with all of the items of the first list, then the second
>>> smoosh(['mama', 'llama'], ['baba', 'yaga'])
['mama', 'llama', 'baba', 'yaga']


# Use a default argument to allow the user to reverse the order!
>>> smoosh(['mama', 'llama'], ['baba', 'yaga'], reverse=True)
['yaga', 'baba', 'llama', 'mama']


"""

def backwards(lst):
    lst = lst[::-1]
    return lst

def maximus(lst):
    max_number = max(lst)
    empty = []
    for i in lst:
        if str(max_number)[0] in str(i):
            new_number = max_number
            empty.append(new_number)
        else:
            empty.append(i)
    return empty

def two_lists(list_one, list_two):
    if list_one[0] == list_two[0]:
        return True
    else:
        return False

def compare_second_letter(list_one, list_two):
    if list_one[1][0].casefold() == list_two[1][0].casefold():
        return True
    else:
        return False

def smoosh(list_one, list_two, reverse=True):
    if reverse == True:
        new_list = list_two[::-1]
        new_list.append(list_one[1])
        return new_list
    else:
        for i in list_two:
            list_one.append(i)
        return list_one
