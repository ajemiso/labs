"""
>>> advise_player(10, 5)
15 Hit.

>>> advise_player('Q', 5)
15 Hit.

>>> advise_player('A', 'K')
21 Blackjack!

>>> advise_player('A', 'K')
21

>>> advise_player('J', 'K')
20 Stay.


# A little harder
Add a Menu to the game.
Put the game in a loop or recursive funciton call, and continue to play until the player types 'exit'.

# Advanced:
How can we accept any number of player cards as argumets, and still advise the player?.

# Super Advanced
Implement score keeping.
Implement Double Down.


"""
def advise_player(card_one, card_two):
    deck = {'a': 11, 'k': 10, 'q': 10, 'j': 10}
    card_one = card_one.lower()
    card_two = card_two.lower()
    if card_one in deck and card_two in deck:
        card_one = deck.get(card_one)
        card_two = deck.get(card_two)
        score = card_one + card_two
        return score
    elif card_one in deck:
        card_one = deck.get(card_one)
        score = card_one + card_two
        return score
    elif card_two in deck:
        card_two = deck.get(card_two)
        score = card_one + card_two
        return score
    else:
        score = card_one + card_two
        return score
    return "False"
