"""
Write a program that prompts the user for a word in snake_case,
then converts and prints it out in CamelCase. Also do the reverse
conversion.
"""

snake_case = input("Please enter your snake_case: ")
snake_case = snake_case.replace("_", " ") #snake case
snake_case = snake_case.title() #Snake Case
snake_case = snake_case.replace(" ", "") #SnakeCase
print(snake_case)
