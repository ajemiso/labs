"""
>>> a
'music'

>>> b
[17, 28, 42, 31, 12]

>>> spell_out(a)
m
u
s
i
c

>>> spell_out(b)
17
28
42
31
12

>>> first_and_last(b)
17
12

>>> first_and_last(a)
m
c

"""

a = 'music'
b = [17, 28, 42, 31, 12]

def spell_out(word):
    for i in word:
        print(i)

def first_and_last(word):
    print(word[0])
    print(word[len(word)-1])

spell_out('Andre')
first_and_last('Andre')
