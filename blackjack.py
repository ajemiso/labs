"""

Make a Card value class with a suit and a rank, both as strings, and a Hand value class with a list of cards.

"""

class Card:

    def __init__(self):
        self.suit = ""
        self.rank = ""


class Hand:

    def __init__(self):
        self.card_list = list()
