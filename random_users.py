import requests
import json

r = requests.get('http://api.randomuser.me/?results=5')
r = r.text
r = json.loads(r)

results = r['results']

def random_users(results):
    for index, item in enumerate(results):
        title = results[index]['name']['title']
        first_name = results[index]['name']['first']
        last_name = results[index]['name']['last']
        email = results[index]['email']
        username = results[index]['login']['username']
        reg_date = results[index]['registered']
        dob = results[index]['dob']

        line_one = "Name: {} {} {}".format(title, first_name, last_name).title()
        line_two = "Email: {}".format(email)
        line_three = "Username: {}".format(username)
        line_four = "Registration date: {}".format(reg_date)
        line_five = "Birth date: {}".format(dob)
        print(line_one, line_two, line_three, line_four, line_five, sep='\n', end='\n')
#print(r['results'][0]['name'])

random_users(results)
