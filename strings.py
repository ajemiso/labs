"""
# Return the count of a letter in a string.
>>> count_letter('i', 'Antidisestablishmentterianism')
5

>>> count_letter('p', 'Pneumonoultramicroscopicsilicovolcanoconiosis')
2

>>> latest_letter('pneumonoultramicroscopicsilicovolcanoconiosis')
the latest letter is v.


>>> lower_case("SUPER!")
'super!'

>>> crazy_spaces("        NANNANANANA BATMAN        ")
'nannananana batman'

# return the number of letter occourances in a string.
>>> locate('l', 'hello')
[2, 3]
"""

def count_letter(select_letter, word):
    count = 0
    for letter in word:
        if letter.lower() == select_letter.lower():
            count += 1
    return count

def latest_letter(word):
    latest = max(word)
    print('the latest letter is {latest}.'.format(latest=latest))

def lower_case(word):
    return word.lower()

def crazy_spaces(word):
    word = word.strip()
    word = word.lower()
    return word

def locate(letter, word):
    word = word.index(letter)
    return word
