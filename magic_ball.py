import random

def magic_ball():
    answers = ["Try again.", "Sure! Why not?", "Yes", "Maybe", "No",
                "Not possible", "It is a possibility"]
    random_answer = answers[random.randint(0, 6)]
    return random_answer

def game():
    while True:
        choice = input("Type play (p) or quit (q): ")
        choice = choice.lower()

        if choice == "p" or choice == "play":
            question = input("What is your question: ")
            answer = magic_ball()
            print(answer)
            play_again = input("Play again? Y/N: ")
            play_again = play_again.lower()
            if play_again == "y" or play_again == "yes":
                continue
            if play_again == "n" or play_again == "no":
                print("Thanks for playing!")
                break
        elif choice == "q" or choice == "quit":
            break

game()
