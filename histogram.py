"""

multiply the number of characters divided by the number of words by 4.17
# add the number of words divided by the number of sentences multiplied by
# 0.5
# subtract total 21.43

"""

import sys
import re

filename = sys.argv[1]
# number = int(sys.argv[2])
extension = '.txt'

"""def quantify_words(filename, number=0):
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            if number == 0:
                f = f.read()
                my_dict = {}
                f = f.lower().split()
                for i in f:
                    my_dict[i] = my_dict.get(i, 0) + 1
                print(my_dict)
            else:
                count = number
                f = f.read()
                my_dict = {}
                f = f.lower().split()
                while count > number:
                    for i in f:
                        my_dict[i] = my_dict.get(i, 0) + 1
                    count += 1
                print(my_dict)
    except TypeError:
        raise Error("File does not exist")"""


def get_characters(docfile):
    with open(docfile, 'r', encoding='utf-8') as f:
        f = f.read()
        characters = []
        for i in f:
            if i.isalpha() or i.isnumeric():
                characters.append(i)
        characters = len(characters)
    return characters


def get_words(docfile):
    with open(docfile, 'r', encoding='utf-8') as f:
        f = f.read()
        f = re.sub(r'[^a-zA-Z0-9 ]', '', f)
        words = f.split()
        words = len(words)
    return words


def get_sentences(docfile):
    with open(docfile, 'r', encoding='utf-8') as f:
        f = f.read()
        sentences = f.split('\n')
        for i in sentences:
            if i == '':
                sentences.remove('')
        sentences = len(sentences)
    return sentences


def calculate(filename):
    characters = get_characters(filename)
    words = get_words(filename)
    sentences = get_sentences(filename)

    step_one = 4.17 * (characters / words)
    step_two = 0.5 * (words / sentences)
    total = (step_one + step_two) - 21.43
    grade_level = int(total)
    return grade_level

def grade_level(number):

    answers = [
                'College graduate: Very difficult to read. Best understood by university graduates.',
                'College: Difficult to read.', '10th - 12th grade: Fairly difficult to read.',
                '8th & 9th grade: Plain English. Easily understood by 13- to 15-year-old students.',
                '7th grade: Fairly easy to read.',
                '6th grade: Easy to read. Conversational English for consumers.',
                '5th grade: Very easy to read. Easily understood by an average 11-year-old '
                'student.'
                    ]

    extreme = list(range(31))
    difficult = list(range(30, 51))
    fairly_diff = list(range(50, 61))
    plain_english = list(range(60, 71))
    seventh_grade = list(range(70, 81))
    sixth_grade = list(range(80, 91))
    fifth_grade = list(range(90, 101))

    if number in extreme:
        print(answers[0])
    elif number in difficult:
        print(answers[1])
    elif number in fairly_diff:
        print(answers[2])
    elif number in plain_english:
        print(answers[3])
    elif number in seventh_grade:
        print(answers[4])
    elif number in sixth_grade:
        print(answers[5])
    elif number in fifth_grade:
        print(answers[6])
    else:
        print("Something went wrong.  Please try again.")
# quantify_words(filename, number)
grade_level(calculate(filename))
