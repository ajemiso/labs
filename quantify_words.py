"""
Write a function that quanitifies word occourances in a given string.

>>> quantify_words("Red touching black is a friend of Jack, Red touching yellow can kill a fellow.")
a 2
black 1
can 1
fellow. 1
friend 1
is 1
jack 1
kill 1
of 1
red 2
touching 2
yellow 1


>>> quantify_words_again("In the end, it's concluded that the airspeed velocity of a \
(European) unladen swallow is about 24 miles per hour or 11 meters per second.")
(european) 1
11 1
24 1
a 1
about 1
airspeed 1
concluded 1
end 1
hour 1
in 1
is 1
it's 1
meters 1
miles 1
of 1
or 1
per 2
second. 1
swallow 1
that 1
the 2
unladen 1
velocity 1


>>> quantify_words_again_again("Red touching black is a friend of Jack, Red touching yellow can kill a fellow.")
a 2
black 1
can 1
fellow. 1
friend 1
is 1
jack 1
kill 1
of 1
red 2
touching 2
yellow 1
"""

import sys

def quantify_words_number(string, number):
    my_dict = {}
    string = string.lower().split()
    for i in string:
        if i not in my_dict:
            my_dict[i] = 1
        elif i in my_dict:
            my_dict[i] += 1
    for k, v in sorted(my_dict.items()):
        print(k, v)

filename = sys.argv[1]
number = int(sys.argv[2])

def quantify_words_again(string, number):
    with open(filename, 'r', encoding='utf-8') as f:
        string = f.read()
        my_dict = {}
        if number == 0:
            string = string.lower().split()
            for i in string:
                my_dict[i] = my_dict.get(i, 0) + 1
        for k, v in sorted(my_dict.items()):
            print(k, v)
        else:
            count = 0
            string = string.lower().split()
            while count < number:
                for i in string:
                    my_dict[i] = my_dict.get(i, 0) + 1
                    count += 1
        for k, v in sorted(my_dict.items()):
            print(k, v)

quantify_words_again(filename, number)

def quantify_words_again_again(string):
    my_dict = {}
    string = string.lower().split()
    for i in string:
        try:
            my_dict[i] += 1
        except KeyError:
            my_dict[i] = 1
    for k, v in sorted(my_dict.items()):
        print(k, v)
