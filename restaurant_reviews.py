"""

We're going to model a mini version of Yelp. There are local business listings and users can post reviews,
with a rating (1 - 5 points) and some text, of each business.


"""

class Username:

    def __init__(self, username, hometown, *args, **kwargs):

        self.username = username
        self.hometown = hometown



class Business:

    def __init__(self, name, city, *args, **kwargs):

        self.name = name
        self.city = city


class Review:

    def __init__(self, username, business_name, rating, review, *args, **kwargs):

        self.username = username
        self.business_name = business_name
        self.rating = rating
        self.review = review



