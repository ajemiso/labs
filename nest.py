"""
>>> letters
[['a', 'b', 'c'], ['d', 'e', 'f', 'g'], ['h', 'i']]

>>> denest(letters)
['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
"""

letters = [['a', 'b', 'c'],
           ['d', 'e', 'f', 'g'],
           ['h', 'i']]

def denest(letters):
    new_list = []
    for i in letters:
        for letter in i:
            new_list.append(letter)
    return new_list
