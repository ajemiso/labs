
width = int(input("Enter width of wall: "))
height = int(input("Enter height of wall: "))
gallon_cost = int(input("How much is the gallon of paint? "))
one_coat = 400

area = width * height

cost = (area / one_coat) * gallon_cost

print("Cost to complete project: {}".format(int(cost)))
