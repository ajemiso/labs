"""
>>> message = "Ohg, bssvpre, V qvqa'g pngpu gurfr -- gurl ner zl crg svfu naq V whfg\
 oevat gurz urer gb fjvz. Jura gurl'er qbar gurl whzc onpx vagb gur ohpxrg."


decrypt(message)
"""

message = "Ohg, bssvpre, V qvqa'g pngpu gurfr -- gurl ner zl crg svfu naq V whfg\
 oevat gurz urer gb fjvz. Jura gurl'er qbar gurl whzc onpx vagb gur ohpxrg."

def decrypt(message):
    alpha_list = 'abcdefghijklmnopqrstuvwxyz'
    new_message = []
    index = 0

    for i in message:
        if i.isalpha() and i in alpha_list:
            index = alpha_list.find(i)
            if index < 13:
                index = index + 13
                new_message.append(alpha_list[index])
            elif index >= 13:
                index = index - 13
                new_message.append(alpha_list[index])
        elif not i.isalpha():
            new_message.append(i)
    new_message = "".join(new_message)
    return new_message

print(decrypt(message))
