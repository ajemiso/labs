import requests
import re
import urllib.request


rain_data = RainData()

rain_data.open_url()

url = 'http://or.water.usgs.gov/non-usgs/bes/sunnyside.rain'

class RainData:

    def __init__(self, *args, **kwargs):
        self.open_url = open_url(self)

    def open_url(self, url):
        self.url = url
        with urllib.request.urlopen(self.url) as rain:
            # for each line of bytecode, convert to string
            lines = [byte_line.decode('utf-8') for byte_line in rain]

            # replace new line characters
            lines = [line.replace("\n", "") for line in lines]
            # remove any line of string that does not begin with a date
            new_lines = [line for line in lines if re.match('\d.-\w..-\d{4}', line)]

            # convert list to dict, storing key as date, first number as value
            rain_list =  []
            for line in new_lines:
                if re.match('\d.-\w..-\d{4}\s*\d', line):
                    match = re.match('\d.-\w..-\d{4}\s*\d', line)
                    rain_list.append(match.group(0))

            # split whitespace
            rain_dict = dict()
            for item in rain_list:
                new_item = item.split() # [1, 2]
                rain_dict[new_item[0]] = new_item[1]
            return rain_dict

    def daily_total(date):



print(clean_lines(open_url(url)))
