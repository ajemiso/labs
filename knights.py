"""
>>> knights()
'Knights who say Ni!'

To run the tests:

$ python -m doctest knights.py
"""

def knights():
    return 'Knights who say Ni!'
