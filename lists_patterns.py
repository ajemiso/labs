"""
>>> a
'music'
>>> b
[17, 28, 42, 31, 12]

>>> display_indexes(a)
m 0
u 1
s 2
i 3
c 4


>>> parallel(a, b)
m 17
u 28
s 42
i 31
c 12

"""

a = 'music'
b = [17, 28, 42, 31, 12]

def display_indexes(a):
    count = 0
    for i in a:
        print("{} {}".format(i, a.find(a[count])))
        count += 1

def parallel(a, b):
    count = 0
    for i in a:
        print("{} {}".format(i, b[count]))
        count += 1

display_indexes(a)
print("\n")
parallel(a, b)
