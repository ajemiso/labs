"""


"""

import sys
import random
import re

filename = sys.argv[1]

def get_words(docfile):
    with open(docfile, 'r', encoding='utf-8') as f:
        f = f.read()
        f = re.sub(r'[^a-zA-Z0-9 ]', '', f)
        words = f.split()
    return words

def extract(words):
    new_dict = dict()
    for index, item in enumerate(words):
        new_dict[item] = new_dict.get(item, words[index+1:])
    return new_dict

def randomizer(filename):
    word_list = get_words(filename) # split words into list
    word_list = extract(word_list)  # extract to dictionary

    lst = []
    keys = list(word_list.keys())
    #key_list = list(word_list.keys())
    #import pdb; pdb.set_trace()
    #keys = random.sample(key_list, 6)

    #for key in keys:
    #    print(" ".join(word_list[key]))

    for key, value in word_list.items():
        #import pdb; pdb.set_trace()
        random_key = random.randint(0, len(keys)) # set random number
        try:
            choice = keys.pop(random_key) # select a random key for dict
        except IndexError:
            pass
            #import pdb; pdb.set_trace()
        val = word_list[choice] # select a random value set *from* dict
        random_index = random.randint(0, len(val)) # set random number
        try:
            list_val = val[random_index] # select random index number from list
        except IndexError:
            import pdb; pdb.set_trace()
        lst.append(list_val)
    print(lst)

randomizer(filename)
