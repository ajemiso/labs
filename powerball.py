import random

def winning_numbers():
    number_list = (list(range(100)))
    number_list = random.sample(number_list, 7)
    return number_list

def game():
    print("Welcome to the Oregon Powerball!")
    numbers = input("Please enter 5 regular numbers and a Powerball number: ")
    numbers = numbers.replace(" ", "")

    for i in numbers:
         i = int(i)

    # fetch winning numbers
    winners = winning_numbers()
    amount_matched = 0

    for user_number, winning_number in zip(numbers, winners):
        if user_number == winning_number:
            amount_matched += 1

    if amount_matched == 7:
        print("Congratulations! You've won the Powerball!")
    elif amount_matched <= 6:
        print("Wow! You matched {} out of 7 numbers!".format(amount_matched))
        print("Numbers drawn: {}".format(winners[0:6]))
    else:
        print("Not sure what happened there.  Please try again.")

game()
