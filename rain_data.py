import requests
import re
import urllib.request

class RainData:

    def __init__(self, url, *args, **kwargs):
        self.url = url
        self.rain_dict = None


    def get_data(self):
        with urllib.request.urlopen(self.url) as rain:
            # for each line of bytecode, convert to string
            lines = [byte_line.decode('utf-8') for byte_line in rain]

            # replace new line characters
            lines = [line.replace("\n", "") for line in lines]

            # remove any line of string that does not begin with a date
            new_lines = (line for line in lines if re.match('\d.-\w..-\d{4}', line))

            # convert list to dict, storing key as date, first number as value
            rain_list =  list()
            for line in new_lines:
                if re.match('\d.-\w..-\d{4}\s*\d', line):
                    match = re.match('\d.-\w..-\d{4}\s+\S+', line)
                    rain_list.append(match.group(0))

            # split whitespace
            rain_dict = dict()
            for item in rain_list:
                new_item = item.split() # [1, 2]
                rain_dict[new_item[0]] = new_item[1]

            rain_dict = {k: int(v)*0.01 for k, v in rain_dict.items() if v.isnumeric()}
            self.rain_dict = rain_dict
        return self.rain_dict

    @property
    def get_max(self):
        # return the highest value
        try:
            data = self.rain_dict
            max_value = 0
            date = ""
            for k, v in data.items():
                if v > max_value:
                    max_value = v
                    date = k
        except AttributeError:
            raise AttributeError('run get_data first')
        return date, str(max_value) + ' inches'
